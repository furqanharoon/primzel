class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  before_save :ensure_authentication_token
  #before_create :stop_email

  devise :database_authenticatable,:registerable,:recoverable, :rememberable, :trackable, :validatable, :token_authenticatable

  belongs_to :campus


  #def stop_email
   # self.skip_confirmation!
  #end

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end
  private
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.find_by_authentication_token(token)
    end
  end
end
