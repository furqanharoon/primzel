class UsersController < ApplicationController
  api :GET, 'users/check_user', 'Get current user and its fields.'
  error :code => 401, :desc => "Unauthorized"
  param :api_key, String, :desc => 'The api_key. Defaults to redeem-registration.', :required => true
  param :auth_token, String, :desc => 'The token to authenticate user.', :required => true
  def check_user
    sign_out current_user
    #if current_user
        return render :json=> {:success=>true,:message=>"Signout!"}
    #else
        #return render :json=> {:message=>"Not Signout!"}
    #end
  end
end
