class RegistrationsController < Devise::RegistrationsController
  api :POST, 'users', 'Create a new user.'
  description "Get user data after sign_in_count."
  param :api_key, String, :desc => 'The api_key. Defaults to redeem-registration.', :required => true
  param :user, Hash, :desc => 'The new users user data.' do
    param :mobile_number, String, :desc => 'The users mobile number.'
    param :country, String, :desc => 'The users country.'
    param :city, String, :desc => 'The users city.'
    param :email, String, :desc => 'The users email address.', :required => true
    param :password, String, :desc => 'The users password.', :required => true
    param :password_confirmation, String, :desc => 'The users password confirmation.', :required => true
    end
    example 'curl -i -H "Accept: application/json" -d "user[email]=hammad417@gmail.com&user[password]=xxxxxxxx&user[password_confirmation]=xxxxxxxx" -X POST http://localhost:3000/users'
  example '{}'
  def create

    respond_to do |format|
      format.json{
        if params[:api_key].blank? or params[:api_key] != API_KEY
         return render :json => {'errors'=>{'api_key' => 'Invalid'}}.to_json, :status => 401
        end
        build_resource
        resource = User.new user_params
        resource.reset_authentication_token
        if resource.save
          resource.ensure_authentication_token!
          #sign_in("user", resource)
          #sign_out(current_user)
          return render :json=> {:success=>true, :auth_token=>resource.authentication_token, :email=>resource.email, :mobile_number => resource.mobile_number}, :status => 200
        else
          p resource.errors.messages
          error_message = ""
          i = 0
          e = resource.errors.messages[:email]
          if e
            0.upto(0) do |m|
              error_message = error_message + "Email " + e[m] + "\n"
              i += 1
            end
          end
          e = resource.errors.messages[:password]
          if e
            0.upto(e.length - 1) do |m|
              error_message = error_message + "Password" + e[m] + "\n"
              i += 1
            end
          end
          if i == 0
            error_message = "Error in creating user."
          end
          render :json => {'error'=>error_message}.to_json, :status => 403
        end
      }
    end
  end
  api :GET, '/users/show', 'Create a new user.'
  param :api_key, String, :desc => 'The api_key. Defaults to redeem-registration.', :required => true
  param :auth_token, String, :desc => 'Auth token of user'
  def show
    if current_user
        p "Signnn outttttt"
        sign_out(current_user)

    end
  end
  private
  def user_params
    params.require(:user).permit!
  end
end
