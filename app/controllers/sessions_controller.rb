class SessionsController < Devise::RegistrationsController
  # prepend_before_filter :require_no_authentication, :only => [:create ]
  # include Devise::Controllers::InternalHelpers
  #before_filter :authenticate_user!, :only => [:destroy]
  skip_before_filter :require_no_authentication, :only => [:create, :destroy]
  before_filter :after_token_authentication, :only => [:create]
  # before_filter :ensure_params_exist , :only => [:create]
  respond_to :json
  api :POST, 'users/sign_in', 'Allows a login through combination of email and password.'
  error :code => 401, :desc => "Unauthorized"
  description "Logs in a user using an email/password combination."
  param :api_key, String, :desc => 'The api_key. Defaults to primzel-registration.', :required => true
  param :user, Hash, :desc => 'Credentials' do
    param :email, String, :desc => 'The users email to login.', :required => true
    param :password, String, :desc => 'The users password to login.', :required => true
  end
  example 'curl -i -H "Accept: application/json" -d "user[email]=hammad@gmail.com&user[password]=password" -X POST http://localhost:3000/users/sign_in'

  def create
    # params[:user] = eval params[:user]
    #resource = User.find_for_database_authentication(:email=>params[:user][:email])
    #return invalid_login_attempt unless resource
    sign_out(current_user)
    if !params[:api_key].blank? and params[:api_key] == API_KEY
      resource = User.find_for_database_authentication(:email=>params[:user][:email])
      return invalid_login_attempt unless resource
      if resource.valid_password?(params[:user][:password])
        resource.ensure_authentication_token!
        sign_in("user", resource)
        resource.ensure_authentication_token!
        #if resource.status_cd == 0
         # sign_out(resource)
          #return render :json=> {:success=>false, :message=>"You cannot login. Your account has been suspended"}, :status=>401
        #else
        return render :json=> {:success=>true, :auth_token=>resource.authentication_token, :email=>resource.email, :user => resource}
        #end
      end
      invalid_login_attempt
    else
      render :json => {'errors'=>{'api_key' => 'Invalid'}}.to_json, :status => 401
    end
  end
  api :POST, 'users/sign_out', 'Performs a logout for the current user.'
  error :code => 401, :desc => "Unauthorized"
  description "Deletes the current authentication_token for the logged in user."
  param :api_key, String, :desc => 'The api_key. Defaults to redeem-registration.', :required => true
  param :auth_token, String, :desc => 'The token to logout.', :required => true
  example 'curl -i -H "Accept: application/json" -d "_method=DELETE&auth_token=xeFxSm7dFhP1T3kVWYGw" -X DELETE http://localhost:3000/users/sign_out'
  def destroy
    if !params[:api_key].blank? and params[:api_key] == API_KEY
      @user=User.where(:authentication_token=>params[:auth_token]).first
      @user.reset_authentication_token!
      return render :json => { :message => ["Session deleted."] },  :success => true, :status => :ok
    else
      render :json => {'errors'=>{'api_key' => 'Invalid'}}.to_json, :status => 401
    end
  end

  protected
  def ensure_params_exist
    return unless params[:user][:email].blank?
    render :json=>{:success=>false, :message=>"missing user_login parameter"}, :status=>422
  end
  def invalid_login_attempt
    warden.custom_failure!
    render :json=> {:success=>false, :message=>"Error with your login or password"}, :status=>401
  end
end
