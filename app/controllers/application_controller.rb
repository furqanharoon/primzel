class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  #protect_from_forgery
  before_filter :after_token_authentication

  def authenticate_user_from_token!
    user_token = params[:user_token].presence
    user       = user_token && User.find_by_authentication_token(user_token.to_s)
    if user
      sign_in user, store: false
    end
  end

  def after_token_authentication
    if params[:auth_token].present?
      @user = User.find_by_authentication_token(params[:auth_token])
      if @user # we are siging in user if it exist. sign_in is devise method
        sign_in @user
      else
        invalid_login_attempt
      end
      #redirect_to root_path # now we are redirecting the user to root_path i,e our home page
    end
  end

  def invalid_login_attempt
    warden.custom_failure!
    render :json=> {:success=>false, :message=>"Unauthorized"}, :status=>401
  end
end