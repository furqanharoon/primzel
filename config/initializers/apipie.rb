Apipie.configure do |config|
  config.app_name                = "Primzel"
  config.api_base_url            = "/"
  config.doc_base_url            = "/doc"
  # where is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/*.rb"
  config.authenticate = Proc.new do
    authenticate_or_request_with_http_basic do |username, password|
        username == "staging" && password == "staging"
    end
  end
end
