class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :content
      t.integer :score
      t.integer :user_id
      t.integer :campus_id

      t.timestamps
    end
  end
end
